package LambdaEq;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//  STREAM API
public class Demo {
	public static void main(String[] args) {
		
	//	List<Integer> list= new ArrayList<>();
	//	list.add(4);
	//	list.add(8);
	//	list.add(27);
	//	list.add(77);
	//	list.add(87);
	//	list.add(78);
		//List<Integer> list=List.of(1,3,5,8,9,12,18,23); // we can add elements by using of() method also by java
		List<Integer> list=Arrays.asList(1,3,5,8,9,12,18,23); 
		
		// list.add(22);   we cant add if we once added using of() method //1.8 new feature  , 
		//but this method has a demerit it is immutable
		List<Integer> list2= new ArrayList<>();
		for(Integer i:list) {
			
			if(i%2==0)            // to get the even values
				list2.add(i);
		}
		for(Integer i:list2)
				
			System.out.println(i);
	}

}
